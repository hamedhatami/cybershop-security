### Create Shared network between Postgres and Keycloak 

     docker network create keycloak-network

### Run the latest version of Postgres Container

     docker run -d --name postgres --net keycloak-network -e POSTGRES_DB=keycloak -e POSTGRES_USER=keycloak -e POSTGRES_PASSWORD=password postgres


### Run the latest version of Keycloak Container

     docker run -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin --name keycloak -p 8080:8080 --net keycloak-network jboss/keycloak


### How to Setup Keycloak with pre-defined JSON

     First of all, you have to go to the administration console of keycloak as follows : 
     http://localhost:8080/auth  (username : admin &&  password : admin)
     At the top left corner of main page, you can find add realm blue button.
     Add a realm with name of "CyberDesign" and then in the left menu, you can find import hyperlink
     Press on the import hyperlink so you can select your JSON (keycloak_scheme.json) file as your 
     pre-defined configuration after a while, your configuration has been setup


### Important Notice

     Press on the Client hyperlink in left menu, A data grid has been shown so you have to select "CyberDesign-basic-client" go furthur then turn on 
     the "Authorization Enabled" then press save button at the end of page.
     Don't leave this page,a new tab will be revealed with name of "Credentials", in this page you just need to pick the secret content 
     (for example : "cffc570d-788b-4766-8cff-e4a80cb7239c") up and after that in a new tab of your browser go to this page "https://codebeautify.org/base64-encode"
     use this formula to get your base64 basic authentication   for example : "CyberDesign-basic-client:cffc570d-788b-4766-8cff-e4a80cb7239c" 
     (Client_name:Client_secret), eventually copy and paste your base64 (for example : "Q3liZXJEZXNpZ24tYmFzaWMtY2xpZW50OmNmZmM1NzBkLTc4OGItNDc2Ni04Y2ZmLWU0YTgwY2I3MjM5Yw==")
     that has been generated by this site into your authorization header as follows


### Simple command to get Token from Keycloak and then send it to your Service

     export TKN=$(curl --insecure 'http://localhost:8080/auth/realms/CyberDesign/protocol/openid-connect/token' \
      -H "Content-Type: application/x-www-form-urlencoded" \
      -H "Authorization: Basic Q3liZXJEZXNpZ24tYmFzaWMtY2xpZW50OmNmZmM1NzBkLTc4OGItNDc2Ni04Y2ZmLWU0YTgwY2I3MjM5Yw==" \
      -d 'username=bob' \
      -d 'password=bob' \
      -d 'grant_type=password' \
      -d 'client_id=CyberDesign-basic-client' | jq -r '.access_token')

     curl -v -X GET 'http://localhost:8081/v1/subscriptions/123456789012345/events?limit=abc' \
     -H "Accept: application/json" \
     -H "Authorization: Bearer $TKN"

### Simple command to introspect Token from Keycloak and then send it to your Service

     export TKN=$(curl --insecure 'http://localhost:8080/auth/realms/CyberDesign/protocol/openid-connect/token' \
      -H "Content-Type: application/x-www-form-urlencoded" \
      -H "Authorization: Basic Q3liZXJEZXNpZ24tYmFzaWMtY2xpZW50OmNmZmM1NzBkLTc4OGItNDc2Ni04Y2ZmLWU0YTgwY2I3MjM5Yw==" \
      -d 'username=bob' \
      -d 'password=bob' \
      -d 'grant_type=password' \
      -d 'client_id=CyberDesign-basic-client' | jq -r '.access_token')

     curl -X POST 'http://localhost:8080/auth/realms/CyberDesign/protocol/openid-connect/token/introspect' \
     -H "Authorization: Basic Q3liZXJEZXNpZ24tYmFzaWMtY2xpZW50OmNmZmM1NzBkLTc4OGItNDc2Ni04Y2ZmLWU0YTgwY2I3MjM5Yw==" \
     -H "Content-Type: application/x-www-form-urlencoded" \
     -d "token=$TKN



### Using Client Jar file 

     There are two major ways of using this quite nice jar file as follows :
     
     1- First Usage : using TokenIntrospector class anywhere you want as a static class which is consist of two methods

          public static String getToken(String username, String password);
          public static JWT tokenIntrospection(String token);

     2- Second Usage : using as a standard JAX-RS Request Filter by putting @Secured on your methods then automatically request 
       filter will be activated and it's going to filter your requests


###  Important note about Jar file

     This jar file is used to use the configuration which it needs in Microprofile Satndard way so if you put this requirements into your 
     project classpath, you will not have problem to use it. (for example : System.setProperties("service.security.keycloak.client_id", "CyberDesign-basic-client"))

     Properties which you have to set before use this jar file : 
     
     service.security.keycloak.client_id   // Client_Id of your realm keycloak ( CyberDesign-basic-client )
     service.security.keycloak.client_secret  // secret that I mentioned above
     service.security.keycloak.host   // host of keycloak ( localhost )
     service.security.keycloak.port   // port number of keycloak ( 8080 )
     service.security.keycloak.realm  // name of your realm ( CyberDesign )

		
 






